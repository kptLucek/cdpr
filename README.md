## Requirements

## Installation

```bash
docker-compose up -d
docker-compose exec php-fpm composer install
docker-compose exec php-fpm ./bin/console doctrine:migrations:migrate --no-interaction --env=dev
```

## Testing

```bash
docker-compose up -d
docker-compose exec php-fpm ./bin/console doctrine:migrations:migrate --no-interaction --env=test
docker-compose exec php-fpm ./vendor/bin/phpunit
```

## Browsing

```bash
docker-compose up -d
```
Go to `http://localhost:9696/api/cards` in your browser

"Public" api has no token requirement, in order to access "private" parts of API you need to provide `X-AUTH-TOKEN` with either `user` or `admin` as it's value.
