<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version2 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO card (id, name, power) VALUES (1, \'Geralt\', 10)');
        $this->addSql('INSERT INTO card (id, name, power) VALUES (2, \'Ciri\', 9)');
        $this->addSql('INSERT INTO card (id, name, power) VALUES (3, \'Vesemir\', 5)');
        $this->addSql('INSERT INTO card (id, name, power) VALUES (4, \'Triss\', 3)');
        $this->addSql('INSERT INTO card (id, name, power) VALUES (5, \'Aard sign\', 0)');
    }

    public function down(Schema $schema) : void
    {
    }
}
