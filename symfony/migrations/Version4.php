<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version4 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `user` (id, username, password, roles) VALUES (1, \'user\', \'password\', \'ROLE_USER\')');
        $this->addSql('INSERT INTO `user` (id, username, password, roles) VALUES (2, \'admin\', \'password\', \'ROLE_ADMIN\')');
        $this->addSql('INSERT INTO `access_token` (user_id, token) VALUES (1, \'user\')');
        $this->addSql('INSERT INTO `access_token` (user_id, token) VALUES (2, \'admin\')');
    }

    public function down(Schema $schema) : void
    {
    }
}
