<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\ApiFormHandler;
use App\Model\AbstractCollectionModel;
use App\Model\ModelInterface;
use App\Model\PaginationRequest;
use App\Transformer\RepresentationTransformer;
use Hateoas\Representation\PaginatedRepresentation;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

abstract class AbstractApiController
{
    protected FormFactoryInterface               $formFactory;
    protected RouterInterface                    $router;
    protected SerializerInterface                $serializer;
    protected AuthorizationCheckerInterface      $authorizationChecker;
    protected TokenStorageInterface              $tokenStorage;
    protected RepresentationTransformer          $transformer;
    protected ApiFormHandler                     $formHandler;

    public function setFormFactory(FormFactoryInterface $formFactory): void
    {
        $this->formFactory = $formFactory;
    }

    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }

    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    public function setAuthorizationChecker(AuthorizationCheckerInterface $authorizationChecker): void
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function setTransformer(RepresentationTransformer $transformer): void
    {
        $this->transformer = $transformer;
    }

    public function setFormHandler(ApiFormHandler $formHandler): void
    {
        $this->formHandler = $formHandler;
    }

    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    protected function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();

        if (true === $token instanceof AnonymousToken) {
            return null;
        }

        $tokenStoredUser = $token->getUser();

        return $tokenStoredUser instanceof User ? $tokenStoredUser : null;
    }

    protected function denyUnlessGranted(string $attribute, $subject = null, string $message = null)
    {
        if (false === $this->authorizationChecker->isGranted($attribute, $subject)) {
            throw new AccessDeniedException($message ?? 'Access Denied.');
        }
    }

    protected function representationResponse(ModelInterface $model, int $statusCode = Response::HTTP_OK, array $customHeaders = []): Response
    {
        $representation           = $this->transformer->transform($model);
        $serializedRepresentation = $this->serializer->serialize($representation, 'json');

        return new JsonResponse($serializedRepresentation, $statusCode, $customHeaders, true);
    }

    protected function paginatedRepresentationResponse(AbstractCollectionModel $model, PaginationRequest $paginationRequest, int $total): Response
    {
        $request                  = $paginationRequest->getRequest();
        $representation           = new PaginatedRepresentation(
            $this->transformer->transform($model),
            $request->get('_route'),
            $request->get('_route_params'),
            $paginationRequest->getPage(),
            $paginationRequest->getLimit(),
            (int) (0 === $total ? 0 : ceil($total / $paginationRequest->getLimit())),
            'page',
            'limit',
            false,
            $total
        );
        $serializedRepresentation = $this->serializer->serialize($representation, 'json');

        return new JsonResponse($serializedRepresentation, Response::HTTP_OK, [], true);
    }

    public function emptyResponse(int $statusCode = Response::HTTP_ACCEPTED): Response
    {
        return new JsonResponse(null, $statusCode);
    }
}
