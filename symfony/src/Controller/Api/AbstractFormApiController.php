<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Model\ModelInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractFormApiController extends AbstractApiController
{
    abstract protected function getMethod(): string;

    abstract protected function getModel(Request $request): ModelInterface;

    abstract protected function getFormClassName(): string;

    protected function getForm(Request $request, ModelInterface $model): FormInterface
    {
        return $this->formFactory->create($this->getFormClassName(), $model, ['method' => $this->getMethod()]);
    }

    public function __invoke(Request $request): Response
    {
        $model = $this->getModel($request);
        $form  = $this->getForm($request, $model);
        $this->preHandle($form, $model);
        $this->formHandler->handle($form, $request);
        $this->postHandle($form, $model);

        return $this->representationResponse($model);
    }

    protected function postHandle(FormInterface $form, ModelInterface $model): void
    {
    }

    protected function preHandle(FormInterface $form, ModelInterface $model): void
    {
    }
}
