<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Card;

use App\Controller\Api\AbstractApiController;
use App\Exception\CardNotFoundException;
use App\Manager\CardManager;
use App\Repository\CardRepository;
use App\Security\Voter\DeleteCardVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteAction extends AbstractApiController
{
    private CardManager    $cardManager;
    private CardRepository $repository;

    public function __construct(CardManager $cardManager, CardRepository $repository)
    {
        $this->cardManager = $cardManager;
        $this->repository  = $repository;
    }

    public function __invoke(int $id): Response
    {
        $model = $this->repository->find($id);

        if (null === $model) {
            throw CardNotFoundException::createForCardNotFound($id);
        }

        $this->denyUnlessGranted(DeleteCardVoter::PERMISSION, $model);

        $this->cardManager->remove($model);

        return $this->emptyResponse();
    }

}
