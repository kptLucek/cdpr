<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Card;

use App\Controller\Api\AbstractApiController;
use App\Controller\Api\AbstractFormApiController;
use App\Entity\Card;
use App\Form\Type\Card\EditType;
use App\Form\Type\Card\NewType;
use App\Manager\CardManager;
use App\Model\ModelInterface;
use App\Security\Voter\AddCardVoter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewAction extends AbstractFormApiController
{
    private CardManager $cardManager;

    public function __construct(CardManager $cardManager)
    {
        $this->cardManager = $cardManager;
    }

    protected function getMethod(): string
    {
        return Request::METHOD_POST;
    }

    protected function getModel(Request $request): ModelInterface
    {
        return $this->cardManager->newInstance();
    }

    protected function getFormClassName(): string
    {
        return NewType::class;
    }

    protected function preHandle(FormInterface $form, ModelInterface $model): void
    {
        $this->denyUnlessGranted(AddCardVoter::PERMISSION);
    }

    protected function postHandle(FormInterface $form, ModelInterface $model): void
    {
        /** @var Card $model */
        $this->cardManager->save($model);
    }
}
