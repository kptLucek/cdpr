<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Card;

use App\Controller\Api\AbstractFormApiController;
use App\Entity\Card;
use App\Exception\CardNotFoundException;
use App\Form\Type\Card\PatchType;
use App\Manager\CardManager;
use App\Model\ModelInterface;
use App\Repository\CardRepository;
use App\Security\Voter\PatchCardVoter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PatchAction extends AbstractFormApiController
{
    private CardManager    $cardManager;
    private CardRepository $repository;

    public function __construct(CardManager $cardManager, CardRepository $repository)
    {
        $this->cardManager = $cardManager;
        $this->repository  = $repository;
    }

    protected function getMethod(): string
    {
        return Request::METHOD_PATCH;
    }

    protected function getModel(Request $request): ModelInterface
    {
        $id    = $request->attributes->getInt('id');
        $model = $this->repository->find($id);

        if (null === $model) {
            throw CardNotFoundException::createForCardNotFound($id);
        }

        $this->denyUnlessGranted(PatchCardVoter::PERMISSION, $model);

        return $model;
    }

    protected function getFormClassName(): string
    {
        return PatchType::class;
    }

    protected function postHandle(FormInterface $form, ModelInterface $model): void
    {
        /** @var Card $model */
        $this->cardManager->save($model);
    }
}
