<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Deck;

use App\Controller\Api\AbstractApiController;
use App\Exception\CardLimitException;
use App\Exception\CardNotFoundException;
use App\Exception\DeckLimitException;
use App\Manager\DeckManager;
use App\Repository\CardRepository;
use App\Repository\DeckRepository;
use App\Security\Voter\AddCardToDeckCardVoter;
use App\Security\Voter\AddCardToDeckVoter;

class AddCardToDeckAction extends AbstractApiController
{
    private CardRepository $cardRepository;
    private DeckRepository $deckRepository;
    private DeckManager    $deckManager;

    public function __construct(CardRepository $cardRepository, DeckRepository $deckRepository, DeckManager $deckManager)
    {
        $this->cardRepository = $cardRepository;
        $this->deckRepository = $deckRepository;
        $this->deckManager    = $deckManager;
    }

    public function __invoke(int $cardId)
    {
        if (false === $this->authorizationChecker->isGranted(AddCardToDeckVoter::PERMISSION)) {
            throw DeckLimitException::createForLimitReached();
        }

        $card = $this->cardRepository->find($cardId);

        if (null === $card) {
            throw CardNotFoundException::createForCardNotFound($cardId);
        }

        if (false === $this->authorizationChecker->isGranted(AddCardToDeckCardVoter::PERMISSION, $card)) {
            throw CardLimitException::createForCardLimitReached();
        }

        $deck = $this->deckRepository->getUserDeck($this->getUser());
        $this->deckManager->addCardToDeck($deck, $card);
        $this->deckManager->save($deck);

        return $this->emptyResponse();
    }
}
