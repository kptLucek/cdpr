<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Deck;

use App\Controller\Api\AbstractApiController;
use App\Exception\DeckNotFoundException;
use App\Model\DeckCardCollectionModel;
use App\Provider\CardProvider;
use App\Repository\DeckRepository;
use Symfony\Component\HttpFoundation\Response;

class CardsAction extends AbstractApiController
{
    private DeckRepository     $deckRepository;
    private CardProvider       $cardProvider;

    public function __construct(DeckRepository $deckRepository, CardProvider $cardProvider)
    {
        $this->deckRepository = $deckRepository;
        $this->cardProvider   = $cardProvider;
    }

    public function __invoke(): Response
    {
        $deck = $this->deckRepository->getUserDeck($this->getUser());

        if (null === $deck) {
            throw new DeckNotFoundException();
        }

        $cards = $this->cardProvider->getUserDeckCards($this->getUser(), $deck);
        $power = $this->deckRepository->fetchDeckPower($deck);

        $model = new DeckCardCollectionModel($cards, $power);

        return $this->representationResponse($model);
    }

}
