<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Deck;

use App\Controller\Api\AbstractApiController;
use App\Manager\DeckManager;
use App\Security\Voter\CreateDeckVoter;
use Symfony\Component\HttpFoundation\Response;

class CreateDeckAction extends AbstractApiController
{
    private DeckManager $deckManager;

    public function __construct(DeckManager $deckManager)
    {
        $this->deckManager = $deckManager;
    }

    public function __invoke(): Response
    {
        $this->denyUnlessGranted(CreateDeckVoter::PERMISSION, null, 'Deck already created');
        $instance = $this->deckManager->newInstance($this->getUser());
        $this->deckManager->save($instance);

        return $this->emptyResponse();
    }

}
