<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Deck;

use App\Controller\Api\AbstractApiController;
use App\Exception\DeckNotFoundException;
use App\Repository\DeckRepository;
use App\Security\Voter\AccessDeckVoter;
use Symfony\Component\HttpFoundation\Response;

class DeckAction extends AbstractApiController
{
    private DeckRepository $repository;

    public function __construct(DeckRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(): Response
    {
        $deck = $this->repository->getUserDeck($this->getUser());

        if (null === $deck) {
            throw new DeckNotFoundException();
        }

        $this->denyUnlessGranted(AccessDeckVoter::PERMISSION, $deck);

        return $this->representationResponse($deck);
    }

}
