<?php
declare(strict_types=1);

namespace App\Controller\Api\PrivateApi\Deck;

use App\Controller\Api\AbstractApiController;
use App\Exception\CardNotFoundException;
use App\Manager\DeckManager;
use App\Repository\CardRepository;
use App\Repository\DeckRepository;
use App\Security\Voter\DeleteCardFromDeckVoter;

class DeleteCardFromDeckAction extends AbstractApiController
{
    private CardRepository $cardRepository;
    private DeckRepository $deckRepository;
    private DeckManager    $deckManager;

    public function __construct(CardRepository $cardRepository, DeckRepository $deckRepository, DeckManager $deckManager)
    {
        $this->cardRepository = $cardRepository;
        $this->deckRepository = $deckRepository;
        $this->deckManager    = $deckManager;
    }

    public function __invoke(int $cardId)
    {
        $deck = $this->deckRepository->getUserDeck($this->getUser());
        $card = $this->cardRepository->find($cardId);

        if (null === $card) {
            throw CardNotFoundException::createForCardNotFound($cardId);
        }

        $this->denyUnlessGranted(DeleteCardFromDeckVoter::PERMISSION, $card, 'You cannot remove this card from your deck.');
        $this->deckManager->removeCardFromDeck($deck, $card);
        $this->deckManager->save($deck);

        return $this->emptyResponse();
    }
}
