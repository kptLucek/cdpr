<?php
declare(strict_types=1);

namespace App\Controller\Api\PublicApi\Card;

use App\Controller\Api\AbstractApiController;
use App\Model\CardCollectionModel;
use App\Model\PaginationRequest;
use App\Repository\CardRepository;
use Symfony\Component\HttpFoundation\Response;

class CollectionAction extends AbstractApiController
{
    private CardRepository $repository;

    public function __construct(CardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(PaginationRequest $paginationRequest): Response
    {
        $cards = $this->repository->paginate($paginationRequest);
        $total = $this->repository->getTotal($paginationRequest);
        $model = new CardCollectionModel($cards);

        return $this->paginatedRepresentationResponse($model, $paginationRequest, $total);
    }
}
