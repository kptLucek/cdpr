<?php
declare(strict_types=1);

namespace App\Controller\Api\PublicApi\Card;

use App\Controller\Api\AbstractApiController;
use App\Repository\CardRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SingleAction extends AbstractApiController
{
    private CardRepository $repository;

    public function __construct(CardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(int $id): Response
    {
        $card = $this->repository->find($id);

        if (null === $card) {
            throw new NotFoundHttpException();
        }

        return $this->representationResponse($card);
    }

}
