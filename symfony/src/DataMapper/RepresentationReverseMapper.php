<?php
declare(strict_types=1);

namespace App\DataMapper;

use App\Entity\Card;
use App\Model\ModelInterface;
use App\Representation\CardRepresentation;
use App\Representation\RepresentationInterface;
use Doctrine\ORM\EntityManagerInterface;

class RepresentationReverseMapper
{
    private EntityManagerInterface $em;
    private array                  $map = [
        CardRepresentation::class => Card::class
    ];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function map(RepresentationInterface $representation, int $identifier): ModelInterface
    {
        $representationClass = get_class($representation);

        if (false === array_key_exists($representationClass, $this->map)) {
            throw new \RuntimeException(sprintf('Unable to process [%s]', $representationClass));
        }

        return $this->em->getReference($this->map[$representationClass], $identifier);
    }
}
