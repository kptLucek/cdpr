<?php
declare(strict_types=1);

namespace App\DependencyInjection\CompilerPass;

use App\Controller\Api\AbstractApiController;
use App\Form\ApiFormHandler;
use App\Transformer\RepresentationTransformer;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class ApiControllerCompilerPass
 * Package App\DependencyInjection\CompilerPass
 */
class ApiControllerCompilerPass implements CompilerPassInterface
{
    const TAG = 'controller.service_arguments';

    public function process(ContainerBuilder $container)
    {
        foreach ($container->findTaggedServiceIds(self::TAG) as $id => $tag) {
            $definition = $container->getDefinition($id);

            try {
                if (true === $definition->isAbstract()) {
                    continue;
                }

                if (null !== $definition->getClass() && false === class_exists($definition->getClass())) {
                    continue;
                }
            } catch (\Throwable $exception) {
                continue;
            }

            if (false === is_subclass_of($definition->getClass(), AbstractApiController::class, true)) {
                continue;
            }

            $definition->addMethodCall('setTransformer', [new Reference(RepresentationTransformer::class)]);
            $definition->addMethodCall('setSerializer', [new Reference(SerializerInterface::class)]);
            $definition->addMethodCall('setFormFactory', [new Reference(FormFactoryInterface::class)]);
            $definition->addMethodCall('setFormHandler', [new Reference(ApiFormHandler::class)]);
            $definition->addMethodCall('setRouter', [new Reference(RouterInterface::class)]);
            $definition->addMethodCall('setTokenStorage', [new Reference(TokenStorageInterface::class)]);
            $definition->addMethodCall('setAuthorizationChecker', [new Reference(AuthorizationCheckerInterface::class)]);
        }
    }
}
