<?php
declare(strict_types=1);

namespace App\Entity;

class AccessToken
{
    private ?int   $id;
    private User   $user;
    private string $token;

    public function __construct(User $user, string $token)
    {
        $this->user  = $user;
        $this->token = $token;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
