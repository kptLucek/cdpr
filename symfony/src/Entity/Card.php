<?php
declare(strict_types=1);

namespace App\Entity;

use App\Model\ModelInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Card implements ModelInterface
{
    private ?int        $id;
    private ?string     $name;
    private ?int        $power;
    private Collection $deckCards;

    public function __construct()
    {
        $this->deckCards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPower(): ?int
    {
        return $this->power;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function setPower(?int $power): void
    {
        $this->power = $power;
    }

    public function getDeckCards(): Collection
    {
        return $this->deckCards;
    }

    public function addDeckCard(DeckCard $deckCard): Card
    {
        if (false === $this->deckCards->contains($deckCard)) {
            $this->deckCards->add($deckCard);
            $deckCard->setCard($this);
        }

        return $this;
    }

    public function removeDeckCard(DeckCard $deckCard): Card
    {
        if (true === $this->deckCards->contains($deckCard)) {
            $this->deckCards->removeElement($deckCard);
        }

        return $this;
    }
}
