<?php
declare(strict_types=1);

namespace App\Entity;

use App\Model\ModelInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Deck implements ModelInterface
{
    private ?int       $id;
    private User       $user;
    private Collection $deckCards;

    public function __construct(User $user)
    {
        $this->user      = $user;
        $this->deckCards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function addDeckCard(DeckCard $deckCard): Deck
    {
        if (false === $this->deckCards->contains($deckCard)) {
            $this->deckCards->add($deckCard);
            $deckCard->setDeck($this);
        }

        return $this;
    }

    public function removeDeckCard(DeckCard $deckCard): Deck
    {
        if (true === $this->deckCards->contains($deckCard)) {
            $this->deckCards->removeElement($deckCard);
        }

        return $this;
    }

    public function getDeckCards(): Collection
    {
        return $this->deckCards;
    }
}
