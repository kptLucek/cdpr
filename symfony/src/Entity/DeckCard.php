<?php
declare(strict_types=1);

namespace App\Entity;

use App\Model\ModelInterface;

class DeckCard implements ModelInterface
{
    private ?int  $id;
    private ?Deck $deck;
    private ?Card $card;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeck(): ?Deck
    {
        return $this->deck;
    }

    public function setDeck(Deck $deck): DeckCard
    {
        $this->deck = $deck;
        $this->deck->addDeckCard($this);

        return $this;
    }

    public function getCard(): ?Card
    {
        return $this->card;
    }

    public function setCard(Card $card): DeckCard
    {
        $this->card = $card;
        $this->card->addDeckCard($this);

        return $this;
    }
}
