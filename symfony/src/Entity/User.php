<?php
declare(strict_types=1);

namespace App\Entity;

use App\Model\ModelInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements ModelInterface, UserInterface
{
    private ?int       $id;
    private ?string    $password;
    private ?string    $username;
    private array      $roles;
    private ?string    $plainPassword;
    private Collection $decks;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->decks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getDecks(): Collection
    {
        return $this->decks;
    }

    public function addDeck(Deck $deck): User
    {
        if (false === $this->decks->contains($deck)) {
            $this->decks->add($deck);
            $deck->setUser($this);
        }

        return $this;
    }

    public function removeDeck(Deck $deck): User
    {
        if (true === $this->decks->contains($deck)) {
            $this->decks->removeElement($deck);
        }

        return $this;
    }
}
