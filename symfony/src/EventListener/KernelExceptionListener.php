<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Exception\CardLimitException;
use App\Exception\CardNotFoundException;
use App\Exception\DeckLimitException;
use App\Exception\DeckNotFoundException;
use App\Exception\InvalidTokenException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class KernelExceptionListener
{
    public function onException(ExceptionEvent $event)
    {
        $throwable = $event->getThrowable();
        switch (get_class($throwable)) {
            case InvalidTokenException::class:
            case UnauthorizedHttpException::class:
                $event->setResponse($this->createResponse($throwable->getMessage(), Response::HTTP_UNAUTHORIZED));
                break;
            case CardLimitException::class:
            case DeckLimitException::class:
            case BadRequestHttpException::class:
                $event->setResponse($this->createResponse($throwable->getMessage(), Response::HTTP_BAD_REQUEST));
                break;
            case UnprocessableEntityHttpException::class:
                $event->setResponse($this->createResponse($throwable->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
                break;
            case AccessDeniedHttpException::class:
                $event->setResponse($this->createResponse($throwable->getMessage(), Response::HTTP_FORBIDDEN));
                break;
            case CardNotFoundException::class:
            case DeckNotFoundException::class:
            case NotFoundHttpException::class:
                $event->setResponse($this->createResponse($throwable->getMessage(), Response::HTTP_NOT_FOUND));
                break;
        }
    }

    private function createResponse(string $errorMessage, int $code): Response
    {
        return new JsonResponse(['error' => $errorMessage], $code);
    }
}
