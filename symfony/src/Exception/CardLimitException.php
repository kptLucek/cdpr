<?php
declare(strict_types=1);

namespace App\Exception;

class CardLimitException extends \RuntimeException
{
    public static function createForCardLimitReached(): CardLimitException
    {
        return new self('You cannot add this card any more.');
    }
}
