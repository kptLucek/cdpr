<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CardNotFoundException extends NotFoundHttpException
{
    public static function createForCardNotFound(int $id): CardNotFoundException
    {
        return new self(sprintf('Card with id [%d] was not found.', $id));
    }
}
