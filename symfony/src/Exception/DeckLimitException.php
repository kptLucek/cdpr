<?php
declare(strict_types=1);

namespace App\Exception;

class DeckLimitException extends \RuntimeException
{
    public static function createForLimitReached(): DeckLimitException
    {
        return new self('Unable to store more cards in single deck.');
    }
}
