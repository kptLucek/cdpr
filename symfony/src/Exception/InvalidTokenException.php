<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidTokenException extends UnauthorizedHttpException
{
    public static function createForInvalidToken(): InvalidTokenException
    {
        return new self('X-AUTH-TOKEN', 'Invalid token provided.');
    }
}
