<?php
declare(strict_types=1);

namespace App\Form\Type\Card;

use App\Entity\Card;
use App\Form\Type\AbstractApiType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditType extends AbstractApiType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'required'    => true,
                'constraints' => [
                    new NotBlank()
                ]
            ]
        );

        $builder->add(
            'power',
            NumberType::class,
            [
                'scale'       => 0,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(['value' => 0])
                ]
            ]
        );
    }

    protected function postSetDefaults(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'  => Card::class,
                'constraints' => [
                    new UniqueEntity(['fields' => ['name'], 'errorPath' => 'name'])
                ]
            ]
        );
    }
}
