<?php
declare(strict_types=1);

namespace App\Form\Type\Card;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PatchType extends EditType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $configuredFieldKeys = array_keys($builder->all());

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($configuredFieldKeys) {
                $form     = $event->getForm();
                $data     = $event->getData();
                $dataKeys = array_keys($data);
                foreach (array_diff($configuredFieldKeys, $dataKeys) as $missingProperty) {
                    $form->remove($missingProperty);
                }
            }
        );
    }
}
