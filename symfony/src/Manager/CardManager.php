<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Card;
use App\Repository\CardRepository;
use Doctrine\ORM\EntityManagerInterface;

class CardManager
{
    private EntityManagerInterface $em;
    private CardRepository         $repository;

    public function __construct(EntityManagerInterface $em, CardRepository $repository)
    {
        $this->em         = $em;
        $this->repository = $repository;
    }

    public function newInstance(): Card
    {
        return new Card();
    }

    public function save(Card $card): void
    {
        $this->em->persist($card);
        $this->em->flush();;
    }

    public function remove(Card $card): void
    {
        $this->em->remove($card);
        $this->em->flush();
    }
}
