<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Card;
use App\Entity\Deck;
use App\Entity\DeckCard;
use App\Repository\DeckCardRepository;
use Doctrine\ORM\EntityManagerInterface;

class DeckCardManager
{
    private DeckCardRepository     $repository;
    private EntityManagerInterface $em;

    public function __construct(DeckCardRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em         = $em;
    }

    public function save(DeckCard $deckCard): void
    {
        $this->em->persist($deckCard);
        $this->em->flush();;
    }

    public function remove(DeckCard $deckCard): void
    {
        $this->em->remove($deckCard);
        $this->em->flush();
    }

    public function newInstance(Deck $deck, Card $card): DeckCard
    {
        $deckCard = new DeckCard();
        $deckCard->setDeck($deck);
        $deckCard->setCard($card);

        return $deckCard;
    }

    public function removeOneCardFromDeck(Deck $deck, Card $card): void
    {
        $deckCard = $this->repository->findOneBy(['card' => $card, 'deck' => $deck]);
        $this->remove($deckCard);
    }
}
