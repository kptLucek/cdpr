<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Card;
use App\Entity\Deck;
use App\Entity\User;
use App\Repository\DeckRepository;
use Doctrine\ORM\EntityManagerInterface;

class DeckManager
{
    private EntityManagerInterface $em;
    private DeckRepository         $repository;
    private DeckCardManager        $deckCardManager;

    public function __construct(EntityManagerInterface $em, DeckRepository $repository, DeckCardManager $deckCardManager)
    {
        $this->em              = $em;
        $this->repository      = $repository;
        $this->deckCardManager = $deckCardManager;
    }

    public function newInstance(User $user): Deck
    {
        return new Deck($user);
    }

    public function save(Deck $deck): void
    {
        $this->em->persist($deck);
        $this->em->flush();;
    }

    public function addCardToDeck(Deck $deck, Card $card): void
    {
        $instance = $this->deckCardManager->newInstance($deck, $card);
        $this->deckCardManager->save($instance);
        $this->save($deck);
    }

    public function removeCardFromDeck(Deck $deck, Card $card): void
    {
        $this->deckCardManager->removeOneCardFromDeck($deck, $card);
    }
}
