<?php
declare(strict_types=1);

namespace App\Model;

class DeckCardCollectionModel extends AbstractCollectionModel
{
    private int $power;

    public function __construct(array $resources, int $power)
    {
        parent::__construct($resources);
        $this->power = $power;
    }

    public function getPower(): int
    {
        return $this->power;
    }
}
