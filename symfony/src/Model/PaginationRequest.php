<?php
declare(strict_types=1);

namespace App\Model;

use Symfony\Component\HttpFoundation\Request;

class PaginationRequest
{
    private int     $page;
    private int     $limit;
    private Request $request;

    public function __construct(Request $request, int $page = 1, int $limit = 3)
    {
        $this->request = $request;
        $this->page    = $page;
        $this->limit   = $limit;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }
}
