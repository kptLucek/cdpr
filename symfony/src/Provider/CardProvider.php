<?php
declare(strict_types=1);

namespace App\Provider;

use App\Entity\Deck;
use App\Entity\DeckCard;
use App\Entity\User;
use App\Repository\DeckCardRepository;

class CardProvider
{
    private DeckCardRepository $repository;

    public function __construct(DeckCardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getUserDeckCards(User $user, Deck $deck): array
    {
        $deckCards = $this->repository->getUserDeckCards($user, $deck);

        return array_map(fn(DeckCard $deckCard) => $deckCard->getCard(), $deckCards);
    }
}
