<?php
declare(strict_types=1);

namespace App\Provider;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{

    public function loadUserByUsername(string $username)
    {

    }

    public function refreshUser(UserInterface $user)
    {

    }

    public function supportsClass(string $class)
    {

    }
}
