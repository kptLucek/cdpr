<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\AccessToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AccessTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    public function findByToken(string $token): ?AccessToken
    {
        $qb = $this->createQueryBuilder('q')
            ->andWhere('q.token = :token')->setParameter('token', $token);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
