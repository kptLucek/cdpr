<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Card;
use App\Model\PaginationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CardRepository extends ServiceEntityRepository implements PaginatedRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Card::class);
    }

    public function paginate(PaginationRequest $request): array
    {
        $qb = $this->createQueryBuilder('q')
                   ->setMaxResults($request->getLimit())
                   ->setFirstResult(($request->getPage() - 1) * $request->getLimit())
        ;

        return $qb->getQuery()->getResult();
    }

    public function getTotal(PaginationRequest $request): int
    {
        $qb = $this->createQueryBuilder('q')
            ->select('COUNT(q)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }
}
