<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Deck;
use App\Entity\DeckCard;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DeckCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeckCard::class);
    }

    public function getUserDeckCards(User $user, Deck $deck): array
    {
        $qb = $this->createQueryBuilder('q')
                        ->leftJoin('q.deck', 'd')
                        ->andWhere('d.user = :user')->setParameter('user', $user)
                        ->andWhere('q.deck = :deck')->setParameter('deck', $deck)
        ;

        return $qb->getQuery()->getResult();
    }
}
