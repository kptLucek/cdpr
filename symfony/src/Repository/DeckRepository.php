<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Card;
use App\Entity\Deck;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DeckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Deck::class);
    }

    public function userHasDeck(User $user): bool
    {
        $qb = $this->createQueryBuilder('q')
                   ->select('COUNT(q)')
                   ->andWhere('q.user = :user')->setParameter('user', $user)
        ;

        return 0 !== (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getUserDeck(User $user): ?Deck
    {
        $qb = $this->createQueryBuilder('q')
                   ->andWhere('q.user = :user')->setParameter('user', $user)
                   ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function fetchDeckPower(Deck $deck): int
    {
        $qb = $this->createQueryBuilder('q')
                   ->select('SUM(c.power)')
                   ->leftJoin('q.deckCards', 'd')
                   ->leftJoin('d.card', 'c')
                   ->where('q = :deck')->setParameter('deck', $deck)
        ;

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function countCardsInDeck(Deck $deck, Card $card = null): int
    {
        $qb = $this->createQueryBuilder('q')
                   ->select('COUNT(d)')
                   ->leftJoin('q.deckCards', 'd')
                   ->andWhere('q = :deck')->setParameter('deck', $deck)
        ;

        if (null !== $card) {
            $qb->andWhere('d.card = :card')->setParameter('card', $card);
        }

        return (int) $qb->getQuery()->getSingleScalarResult();
    }
}
