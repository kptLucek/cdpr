<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\PaginationRequest;

interface PaginatedRepository
{
    public function paginate(PaginationRequest $request): array;

    public function getTotal(PaginationRequest $request): int;
}
