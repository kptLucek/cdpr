<?php
declare(strict_types=1);

namespace App\Representation;

class CardRepresentation implements RepresentationInterface
{
    private int $id;
    private string $name;
    private int $power;

    public function __construct(int $id, string $name, int $power)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->power = $power;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPower(): int
    {
        return $this->power;
    }
}
