<?php
declare(strict_types=1);

namespace App\Representation;

class DeckCardsRepresentation implements RepresentationInterface
{
    private int   $power;
    private array $cards;

    public function __construct(int $power, array $cards)
    {
        $this->power = $power;
        $this->cards = $cards;
    }

    public function getPower(): int
    {
        return $this->power;
    }

    public function getCards(): array
    {
        return $this->cards;
    }
}
