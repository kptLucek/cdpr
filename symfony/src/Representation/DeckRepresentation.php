<?php
declare(strict_types=1);

namespace App\Representation;

class DeckRepresentation implements RepresentationInterface
{
    private int                $id;
    private int                $userId;
    private int                $power;
    private array              $cardIds = [];

    public function __construct(int $id, int $userId, int $power)
    {
        $this->id     = $id;
        $this->userId = $userId;
        $this->power  = $power;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getPower(): int
    {
        return $this->power;
    }

    public function getCardIds(): array
    {
        return $this->cardIds;
    }

    public function setCardIds(array $cardIds): DeckRepresentation
    {
        $this->cardIds = $cardIds;

        return $this;
    }
}
