<?php
declare(strict_types=1);

namespace App\Representation;

class UserRepresentation implements RepresentationInterface
{
    private int    $id;
    private string $username;
    private array  $roles;
}
