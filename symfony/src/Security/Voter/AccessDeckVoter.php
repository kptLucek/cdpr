<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\DataMapper\RepresentationReverseMapper;
use App\Entity\Deck;
use App\Entity\User;
use App\Repository\DeckRepository;
use App\Representation\DeckRepresentation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccessDeckVoter extends Voter
{
    const PERMISSION = 'ACCESS_DECK';
    private AccessDecisionManagerInterface $accessDecision;
    private DeckRepository                 $deckRepository;
    private RepresentationReverseMapper    $mapper;

    public function __construct(AccessDecisionManagerInterface $accessDecision, DeckRepository $deckRepository, RepresentationReverseMapper $mapper)
    {
        $this->accessDecision = $accessDecision;
        $this->deckRepository = $deckRepository;
        $this->mapper         = $mapper;
    }

    protected function supports(string $attribute, $subject)
    {
        if (self::PERMISSION !== $attribute) {
            return false;
        }

        return true === $subject instanceof Deck || true === $subject instanceof DeckRepresentation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if (false === $this->accessDecision->decide($token, ['ROLE_USER'])) {
            return false;
        }

        /** @var DeckRepresentation $subject */
        if (true === $subject instanceof DeckRepresentation) {
            $subject = $this->mapper->map($subject, $subject->getId());
        }

        $user = $token->getUser();

        if (false === $user instanceof User) {
            return false;
        }

        /** @var Deck $subject */
        return $subject->getUser() === $user;
    }
}
