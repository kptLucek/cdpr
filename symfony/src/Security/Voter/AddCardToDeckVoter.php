<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Deck;
use App\Entity\User;
use App\Repository\DeckRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AddCardToDeckVoter extends Voter
{
    const PERMISSION = 'ADD_CARD_TO_DECK';
    private AccessDecisionManagerInterface $accessDecision;
    private DeckRepository                 $deckRepository;

    public function __construct(AccessDecisionManagerInterface $accessDecision, DeckRepository $deckRepository)
    {
        $this->accessDecision = $accessDecision;
        $this->deckRepository = $deckRepository;
    }

    protected function supports(string $attribute, $subject)
    {
        return self::PERMISSION === $attribute;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if (false === $this->accessDecision->decide($token, ['ROLE_USER'])) {
            return false;
        }

        $user = $token->getUser();

        if (false === $user instanceof User) {
            return false;
        }

        $deck = $this->deckRepository->getUserDeck($user);

        if (null === $deck) {
            return false;
        }

        /** @var Deck $subject */
        return 10 > $this->deckRepository->countCardsInDeck($deck);
    }
}
