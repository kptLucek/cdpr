<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\DataMapper\RepresentationReverseMapper;
use App\Entity\Card;
use App\Representation\CardRepresentation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AddCardVoter extends Voter
{
    const PERMISSION = 'ADD_CARD';
    private AccessDecisionManagerInterface $accessDecisionManager;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    protected function supports(string $attribute, $subject)
    {
        return self::PERMISSION === $attribute;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        return $this->accessDecisionManager->decide($token, ['ROLE_ADMIN']);
    }
}
