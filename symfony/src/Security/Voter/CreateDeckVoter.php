<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\User;
use App\Repository\DeckRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CreateDeckVoter extends Voter
{
    const PERMISSION = 'CREATE_DECK';
    private AccessDecisionManagerInterface $accessDecisionManager;
    private DeckRepository                 $deckRepository;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager, DeckRepository $deckRepository)
    {
        $this->accessDecisionManager = $accessDecisionManager;
        $this->deckRepository        = $deckRepository;
    }

    protected function supports(string $attribute, $subject)
    {
        return self::PERMISSION === $attribute;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if (false === $this->accessDecisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        $user = $token->getUser();

        if (false === $user instanceof User) {
            return false;
        }

        return false === $this->deckRepository->userHasDeck($user);
    }
}
