<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\DataMapper\RepresentationReverseMapper;
use App\Entity\Card;
use App\Entity\User;
use App\Repository\DeckRepository;
use App\Representation\CardRepresentation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DeleteCardFromDeckVoter extends Voter
{
    const PERMISSION = 'DELETE_CARD_FROM_DECK';
    private AccessDecisionManagerInterface $accessDecisionManager;
    private DeckRepository                 $deckRepository;
    private RepresentationReverseMapper    $mapper;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager, DeckRepository $deckRepository, RepresentationReverseMapper $mapper)
    {
        $this->accessDecisionManager = $accessDecisionManager;
        $this->deckRepository        = $deckRepository;
        $this->mapper                = $mapper;
    }

    protected function supports(string $attribute, $subject)
    {
        if (self::PERMISSION !== $attribute) {
            return false;
        }

        return true === $subject instanceof Card || true === $subject instanceof CardRepresentation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if (false === $this->accessDecisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        if (true === $subject instanceof CardRepresentation) {
            $subject = $this->mapper->map($subject, $subject->getId());
        }

        $user = $token->getUser();

        if (false === $user instanceof User) {
            return false;
        }

        $deck = $this->deckRepository->getUserDeck($user);

        if (null === $deck) {
            return false;
        }

        /** @var Card $subject */
        return 0 < $this->deckRepository->countCardsInDeck($deck, $subject);
    }
}
