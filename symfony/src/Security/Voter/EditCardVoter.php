<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Card;
use App\Representation\CardRepresentation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EditCardVoter extends Voter
{
    const PERMISSION = 'EDIT_CARD';
    private AccessDecisionManagerInterface $accessDecisionManager;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    protected function supports(string $attribute, $subject)
    {
        if (self::PERMISSION !== $attribute) {
            return false;
        }

        return true === $subject instanceof Card || true === $subject instanceof CardRepresentation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        return $this->accessDecisionManager->decide($token, ['ROLE_ADMIN']);
    }
}
