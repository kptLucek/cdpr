<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Model\CardCollectionModel;
use App\Model\ModelInterface;
use App\Representation\CardCollectionRepresentation;
use App\Representation\RepresentationInterface;

class CardCollectionRepresentationTransformer implements RepresentationTransformerInterface
{
    private RepresentationTransformer $transformer;

    public function __construct(RepresentationTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function supports(ModelInterface $model): bool
    {
        return true === $model instanceof CardCollectionModel;
    }

    public function transform(ModelInterface $model): RepresentationInterface
    {
        $data = [];

        /** @var CardCollectionModel $model */
        foreach ($model->getResources() as $resource) {
            $data[] = $this->transformer->transform($resource);
        }

        return new CardCollectionRepresentation($data);
    }
}
