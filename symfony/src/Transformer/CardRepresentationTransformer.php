<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Entity\Card;
use App\Model\ModelInterface;
use App\Representation\CardRepresentation;
use App\Representation\RepresentationInterface;

class CardRepresentationTransformer implements RepresentationTransformerInterface
{
    public function supports(ModelInterface $model): bool
    {
        return true === $model instanceof Card;
    }

    public function transform(ModelInterface $model): RepresentationInterface
    {
        /** @var Card $model */
        return new CardRepresentation($model->getId(), $model->getName(), $model->getPower());
    }
}
