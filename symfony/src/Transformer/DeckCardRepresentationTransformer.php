<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Entity\DeckCard;
use App\Model\ModelInterface;
use App\Representation\RepresentationInterface;

class DeckCardRepresentationTransformer implements RepresentationTransformerInterface
{

    public function supports(ModelInterface $model): bool
    {
        return true === $model instanceof DeckCard;
    }

    public function transform(ModelInterface $model): RepresentationInterface
    {

    }
}
