<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Model\DeckCardCollectionModel;
use App\Model\ModelInterface;
use App\Representation\DeckCardsRepresentation;
use App\Representation\RepresentationInterface;

class DeckCardsRepresentationTransformer implements RepresentationTransformerInterface
{
    private RepresentationTransformer $transformer;

    public function __construct(RepresentationTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function supports(ModelInterface $model): bool
    {
        return true === $model instanceof DeckCardCollectionModel;
    }

    public function transform(ModelInterface $model): RepresentationInterface
    {
        $cards = [];
        /** @var DeckCardCollectionModel $model */
        foreach ($model->getResources() as $resource) {
            $cards[] = $this->transformer->transform($resource);
        }

        return new DeckCardsRepresentation($model->getPower(), $cards);
    }
}
