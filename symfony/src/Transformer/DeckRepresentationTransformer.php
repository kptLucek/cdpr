<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Entity\Deck;
use App\Entity\DeckCard;
use App\Model\ModelInterface;
use App\Repository\DeckCardRepository;
use App\Repository\DeckRepository;
use App\Representation\DeckRepresentation;
use App\Representation\RepresentationInterface;

class DeckRepresentationTransformer implements RepresentationTransformerInterface
{
    private DeckRepository            $deckRepository;
    private DeckCardRepository        $deckCardRepository;
    private RepresentationTransformer $transformer;

    public function __construct(DeckRepository $deckRepository, DeckCardRepository $deckCardRepository, RepresentationTransformer $transformer)
    {
        $this->deckRepository     = $deckRepository;
        $this->deckCardRepository = $deckCardRepository;
        $this->transformer        = $transformer;
    }

    public function supports(ModelInterface $model): bool
    {
        return true === $model instanceof Deck;
    }

    public function transform(ModelInterface $model): RepresentationInterface
    {
        /** @var Deck $model */
        $power               = $this->deckRepository->fetchDeckPower($model);
        $deckRepresentation  = new DeckRepresentation($model->getId(), $model->getUser()->getId(), $power);
        $deckCards           = $this->deckCardRepository->findBy(['deck' => $model]);
        $cardIds             = array_map(fn(DeckCard $card) => $card->getCard()->getId(), $deckCards);
        $deckRepresentation->setCardIds($cardIds);

        return $deckRepresentation;
    }
}
