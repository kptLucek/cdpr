<?php
declare(strict_types=1);

namespace App\Tests;

use ApiTestCase\JsonApiTestCase;

class ApiTestCase extends JsonApiTestCase
{
    protected static array $defaultHeaders = [
        'CONTENT_TYPE' => 'application/json',
    ];

    protected static array $userHeaders = [
        'CONTENT_TYPE' => 'application/json',
        'HTTP_X-AUTH-TOKEN' => 'user'
    ];

    protected static array $adminHeaders = [
        'CONTENT_TYPE' => 'application/json',
        'HTTP_X-AUTH-TOKEN' => 'admin'
    ];

    protected static array $fakeUserHeaders = [
        'CONTENT_TYPE' => 'application/json',
        'HTTP_X-AUTH-TOKEN' => 'fake'
    ];
}
