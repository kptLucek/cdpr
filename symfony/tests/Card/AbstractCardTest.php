<?php
declare(strict_types=1);

namespace App\Tests\Card;

use App\Entity\Card;
use App\Tests\ApiTestCase;

abstract class AbstractCardTest extends ApiTestCase
{
    public function getCardUrl(Card $card): string
    {
        return $this->getCardUrlById($card->getId());
    }

    protected function getCardUrlById(int $id): string
    {
        return sprintf('/api/cards/%d', $id);
    }
}
