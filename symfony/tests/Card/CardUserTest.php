<?php
declare(strict_types=1);

namespace App\Tests\Card;

use Symfony\Component\HttpFoundation\Response;

class CardUserTest extends AbstractCardTest
{
    public function test_card_pagination_when_it_should_respond_with_paginated_data()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/card_collection_paginated_first_page', Response::HTTP_OK);
    }

    public function test_card_pagination_when_it_should_respond_with_paginated_data_on_second_page()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', ['page' => 2], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/card_collection_paginated_second_page', Response::HTTP_OK);
    }

    public function test_card_single_when_it_should_respond_with_single_representation()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('GET', $this->getCardUrl($fixtures['card_2']), [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/card_single', Response::HTTP_OK);
    }

    public function test_card_edit_when_name_and_power_is_provided_it_should_be_denied()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );
        $data     =
<<<EOT
        {
            "name": "This is new card name",
            "power": 75
        }
EOT;
        $this->client->request('PUT', $this->getCardUrl($fixtures['card_2']), [], [], self::$userHeaders, $data);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/generic_access_denied', Response::HTTP_FORBIDDEN);
    }

    public function test_card_patch_when_name_is_provided_it_should_be_denied()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );
        $data     =
            <<<EOT
        {
            "name": "This is new card name"
        }
EOT;
        $this->client->request('PATCH', $this->getCardUrl($fixtures['card_2']), [], [], self::$userHeaders, $data);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/generic_access_denied', Response::HTTP_FORBIDDEN);
    }

    public function test_card_patch_when_power_is_provided_it_should_be_denied()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );
        $data     =
            <<<EOT
        {
            "power": 75
        }
EOT;
        $this->client->request('PATCH', $this->getCardUrl($fixtures['card_2']), [], [], self::$userHeaders, $data);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/generic_access_denied', Response::HTTP_FORBIDDEN);
    }

    public function test_card_remove_when_card_exists_it_should_be_denied()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('DELETE', $this->getCardUrl($fixtures['card_5']), [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/generic_access_denied', Response::HTTP_FORBIDDEN);
    }

    public function test_card_collection_after_card_remove_when_card_exists()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('DELETE', $this->getCardUrl($fixtures['card_5']), [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/generic_access_denied', Response::HTTP_FORBIDDEN);

        $this->client->request('GET', '/api/cards', [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/user/card_collection_paginated_first_page_after_removing', Response::HTTP_OK);
    }

    public function test_card_remove_when_card_does_not_exist()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml'
            ]
        );

        $this->client->request('DELETE', $this->getCardUrlById(6), [], [], self::$userHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'card/admin/card_delete_non_existing_card', Response::HTTP_NOT_FOUND);
    }
}
