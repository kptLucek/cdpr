<?php
declare(strict_types=1);

namespace App\Tests\Deck;

use App\Entity\Card;
use App\Tests\ApiTestCase;

abstract class AbstractDeckTest extends ApiTestCase
{
    public function getDeckCardUrl(Card $card): string
    {
        return $this->getDeckCardUrlById($card->getId());
    }

    protected function getDeckCardUrlById(int $id): string
    {
        return sprintf('/api/deck/%d', $id);
    }
}
