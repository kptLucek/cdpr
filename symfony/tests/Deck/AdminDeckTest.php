<?php
declare(strict_types=1);

namespace App\Tests\Deck;

use Symfony\Component\HttpFoundation\Response;

class AdminDeckTest extends AbstractDeckTest
{
    public function test_card_collection_when_deck_is_created_and_empty_it_should_display_additional_link()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/empty_deck_card_collection_paginated_first_page', Response::HTTP_OK);
    }

    public function test_card_collection_second_page_when_deck_is_created_and_empty_it_should_display_additional_link()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', ['page' => 2], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/empty_deck_card_collection_paginated_second_page', Response::HTTP_OK);
    }

    public function test_add_card_to_deck_it_should_allow()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml'
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/generic_accepted_response', Response::HTTP_ACCEPTED);
    }

    public function test_remove_card_from_deck_it_should_deny()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml'
            ]
        );

        $this->client->request('DELETE', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/remove_card_from_deck_when_is_not_in', Response::HTTP_FORBIDDEN);
    }

    public function test_deck_cards_when_deck_is_empty_and_created_it_should_respond()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml'
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/empty_deck_cards_response', Response::HTTP_OK);
    }

    public function test_deck_when_deck_is_not_empty_and_created_it_should_respond()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_one_entry.yml',
            ]
        );

        $this->client->request('GET', '/api/deck', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_with_single_card', Response::HTTP_OK);
    }

    public function test_deck_cards_when_deck_is_not_empty_and_created_it_should_respond()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_one_entry.yml',
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_cards_with_single_card', Response::HTTP_OK);
    }

    public function test_add_card_to_deck_when_there_is_same_card_already_it_should_allow()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_one_entry.yml',
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/generic_accepted_response', Response::HTTP_ACCEPTED);
    }

    public function test_add_card_to_deck_when_deck_is_full_it_should_deny()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/additional_cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_limit_reached.yml',
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_6']), [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_add_card_when_deck_reached_limit', Response::HTTP_BAD_REQUEST);
    }

    public function test_deck_when_deck_is_full_it_should_respond_with_response_with_valid_points()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/additional_cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_limit_reached.yml',
            ]
        );

        $this->client->request('GET', '/api/deck', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_full', Response::HTTP_OK);
    }

    public function test_deck_cards_when_deck_is_full_it_should_respond_with_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/additional_cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_limit_reached.yml',
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_cards_full', Response::HTTP_OK);
    }

    public function test_cards_collection_when_deck_is_full_it_should_respond_with_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_admin.yml',
                'resources/deck_cards_admin_with_limit_reached.yml',
            ]
        );

        $this->client->request('GET', '/api/cards', [], [], self::$adminHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/admin/deck_full_card_collection_paginated_first_page', Response::HTTP_OK);
    }
}
