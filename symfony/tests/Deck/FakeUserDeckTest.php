<?php
declare(strict_types=1);

namespace App\Tests\Deck;

use Symfony\Component\HttpFoundation\Response;

class FakeUserDeckTest extends AbstractDeckTest
{
    public function test_card_collection_when_deck_is_created_and_empty_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_user.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_card_collection_second_page_when_deck_is_created_and_empty_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_user.yml'
            ]
        );

        $this->client->request('GET', '/api/cards', ['page' => 2], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_add_card_to_deck_it_should_throw_invalid_token_response()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
                'resources/deck_user.yml'
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_remove_card_from_deck_it_should_throw_invalid_token_response()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('DELETE', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_deck_cards_when_deck_is_empty_and_created_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_deck_when_deck_is_not_empty_and_created_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/deck', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_deck_cards_when_deck_is_not_empty_and_created_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_add_card_to_deck_when_there_is_same_card_already_it_should_throw_invalid_token_response()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_1']), [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_add_card_to_deck_when_deck_is_full_it_should_throw_invalid_token_response()
    {
        $fixtures = $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('POST', $this->getDeckCardUrl($fixtures['card_5']), [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_deck_when_deck_is_full_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/deck', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_deck_cards_when_deck_is_full_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/deck/cards', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }

    public function test_cards_collection_when_deck_is_full_it_should_throw_invalid_token_response()
    {
        $this->loadFixturesFromFiles(
            [
                'resources/users.yml',
                'resources/access_tokens.yml',
                'resources/cards.yml',
            ]
        );

        $this->client->request('GET', '/api/cards', [], [], self::$fakeUserHeaders);
        $response = $this->client->getResponse();

        $this->assertResponse($response, 'deck/fake/generic_access_denied', Response::HTTP_UNAUTHORIZED);
    }
}
